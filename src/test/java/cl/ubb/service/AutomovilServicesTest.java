package cl.ubb.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyString;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;



@RunWith(MockitoJUnitRunner.class)
public class AutomovilServicesTest {

	@Mock
	private AutomovilDao autoDao;
	
	@InjectMocks
	private AutomovilServices autoServices;

	@Before
	public void setUp() throws Exception {
	}

		
	@Test
	public void guardarAutoYRetornarlo() {
		
		Automovil auto=new Automovil();
		Automovil auto2=new Automovil();
		
		when(autoDao.save(auto)).thenReturn(auto);
		auto2=autoServices.crearAuto(auto);
		
		Assert.assertNotNull(auto2);
		Assert.assertEquals(auto, auto2);
		
		
	}
	
	@Test
	public void listarAutosPorCategoria() {
		
		List<Automovil> auto=new ArrayList<Automovil>();
		List<Automovil> auto2=new ArrayList<Automovil>();
		
		when(autoDao.findByCategoria(anyString())).thenReturn(auto);
		auto2 = autoServices.obtenerPorCategoria("lujo");
		
		//assert
		Assert.assertNotNull(auto2);
		Assert.assertEquals(auto, auto2);
		
		
	}
	

}
