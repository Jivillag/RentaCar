package cl.ubb.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;


@RunWith(MockitoJUnitRunner.class)
public class ClienteServicesTest {
	
	@Mock
	private ClienteDao clientedao;
		
	@InjectMocks
	private ClienteServices clienteservice;
	

	@Before
	public void SetUp() throws Exception{
		
	}
	
	@Test
	public void guardarClienteYRetornarlo() {
		
		Cliente cliente=new Cliente();
		Cliente cliente2=new Cliente();
		
		when(clientedao.save(cliente)).thenReturn(cliente);
		cliente2=clienteservice.crearCliente(cliente);
		
		Assert.assertNotNull(cliente2);
		Assert.assertEquals(cliente, cliente2);
		
	
	}
	
	@Test
	public void obtenterTodosLosClientes() throws Exception{
		//arrange
		List<Cliente> cliente = new ArrayList<Cliente>();
		List<Cliente> resultado = new ArrayList<Cliente>();
		
		//act
		when(clientedao.findAll()).thenReturn(cliente);
		resultado = clienteservice.obtenerCliente();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, cliente);
	}

	
}
