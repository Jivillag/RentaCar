package cl.ubb.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Automovil;



public interface AutomovilDao extends CrudRepository<Automovil,Long>{

	public List<Automovil> findByCategoria(String categoria);
}
