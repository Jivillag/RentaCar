package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Reserva;



public interface ReservaDao extends CrudRepository<Reserva,Long>{

}
