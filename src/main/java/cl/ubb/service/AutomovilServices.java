package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;

public class AutomovilServices {
	
	private final AutomovilDao autoDao;
	
	@Autowired
	public AutomovilServices(AutomovilDao autoDao){
		
		this.autoDao=autoDao;
		
	}

	public Automovil crearAuto(Automovil auto) {
		// TODO Auto-generated method stub
		return autoDao.save(auto);
	}

	public List<Automovil> obtenerPorCategoria(String string) {
		// TODO Auto-generated method stub
		List<Automovil> auto = new ArrayList<Automovil>();
		auto = (List<Automovil>) autoDao.findByCategoria(string); 
		return auto;
	}

}
